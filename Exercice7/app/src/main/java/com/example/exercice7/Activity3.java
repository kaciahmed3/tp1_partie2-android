package com.example.exercice7;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URI;

public class Activity3 extends AppCompatActivity {

    Button btnCall;
    TextView tvNumTel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);

        ActivityCompat.requestPermissions(Activity3.this,new String[]{Manifest.permission.CALL_PHONE}, 1);
        btnCall=(Button)findViewById(R.id.btn_appel);
        final Bundle bundle=getIntent().getExtras();
        String numTel = bundle.getString(MainActivity.EXTRA_NUM_TEL);
        tvNumTel=(TextView)findViewById(R.id.textView_titre_activite3);
        tvNumTel.setText(getString(R.string.textView_titre_activite3)+"  "+numTel);
        /*
             Lancer l'appel quand on click sur le button
         */
        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentAppel =new Intent(Intent.ACTION_CALL,Uri.parse("tel:"+numTel));
                intentAppel.putExtra(Intent.EXTRA_PHONE_NUMBER,Integer.valueOf(numTel));
                startActivity(intentAppel);
            }
        });
    }
}