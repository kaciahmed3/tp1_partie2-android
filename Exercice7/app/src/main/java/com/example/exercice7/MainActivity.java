package com.example.exercice7;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnValider;
    final Context context =this;
    private EditText edNom;
    private EditText edPrenom;
    private  EditText edAge;
    private EditText edDomComp;
    private EditText edNumTel;
    public static final String EXTRA_NOM="Nom";
    public static final String EXTRA_PRENOM ="Prenom";
    public static final String EXTRA_AGE="Age";
    public static final String EXTRA_DOM_COMP="DomComp";
    public static final String EXTRA_NUM_TEL="NumTel";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnValider=(Button)findViewById(R.id.btn_valider);
        btnValider.setOnClickListener(new View.OnClickListener() {
            private boolean isEmpty(EditText edText) {
                if(edText.getText().toString().trim().length()==0)
                    return true;
                else
                    return false;
            }
            @Override
            public void onClick(View v) {
                /*
                     Récupération du contenu des champs de saisie
                 */
                edNom =(EditText)findViewById(R.id.editText_nom);
                edPrenom = (EditText)findViewById(R.id.editText_prenom);
                edAge=(EditText)findViewById(R.id.editText_age);
                edDomComp=(EditText)findViewById(R.id.editText_dom_comp);
                edNumTel=(EditText)findViewById(R.id.editText_num_tel);
               /*
                    Afficher la boite de dialogue de confirmation si tous les champs sont saisis.
                 */
                if(!this.isEmpty(edNom) && !this.isEmpty(edPrenom) && !this.isEmpty(edAge) && !this.isEmpty(edDomComp) && !this.isEmpty(edNumTel))
                {
                    confirmDialog(v);
                }
                else{
                     /*
                         Modfier la couleur des champs de saisie nom remplie
                     */
                    Toast.makeText(getApplicationContext(),getString(R.string.champsVide),Toast.LENGTH_LONG).show();

                    if(this.isEmpty(edNom))
                        edNom.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    if(this.isEmpty(edPrenom))
                        edPrenom.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    if(this.isEmpty(edAge))
                        edAge.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    if(this.isEmpty(edDomComp))
                        edDomComp.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    if(this.isEmpty(edNumTel))
                        edNumTel.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                }
            }
        });

    }
    /*
       Code de la boite de dialogue de confirmation
   */
    public void confirmDialog(View v){
        AlertDialog.Builder myAlertDialogBuilder = new AlertDialog.Builder(context);
        myAlertDialogBuilder.setTitle(getString(R.string.titre_dial));
        myAlertDialogBuilder.setMessage(getString(R.string.msg_dial));
        myAlertDialogBuilder.setCancelable(false);
        myAlertDialogBuilder.setPositiveButton(getString(R.string.btn_oui), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent intentActivity2= new Intent(context,Activity2.class);

                intentActivity2.putExtra(EXTRA_NOM,edNom.getText().toString());
                intentActivity2.putExtra(EXTRA_PRENOM,edPrenom.getText().toString());
                intentActivity2.putExtra(EXTRA_AGE,edAge.getText().toString());
                intentActivity2.putExtra(EXTRA_DOM_COMP,edDomComp.getText().toString());
                intentActivity2.putExtra(EXTRA_NUM_TEL,edNumTel.getText().toString());

                startActivity(intentActivity2);
                dialog.cancel();
            }
        });
        myAlertDialogBuilder.setNegativeButton(getString(R.string.btn_non), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                 /*
                     Remettre  la couleur de fond des editText en gris
                 */
                edNom.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                edPrenom.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                edAge.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                edDomComp.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                edNumTel.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                dialog.cancel();
            }
        });
        AlertDialog myAlertDialog =myAlertDialogBuilder.create();
        myAlertDialog.show();
    }

}