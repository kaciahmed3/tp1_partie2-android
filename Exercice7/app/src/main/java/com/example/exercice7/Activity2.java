package com.example.exercice7;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class Activity2 extends AppCompatActivity {
    TextView tvNom;
    TextView tvPrenom;
    TextView tvAge;
    TextView tvDomComp;
    TextView tvNumTel;
    Button btnOk;
    Button btnReturn;
    String valNumTel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        /*
            Récupèration des textView de l'activité
         */
        tvNom=(TextView)findViewById(R.id.textView_nom);
        tvPrenom=(TextView)findViewById(R.id.textView_prenom);
        tvAge=(TextView)findViewById(R.id.textView_age);
        tvDomComp=(TextView)findViewById(R.id.textView_dom_com);
        tvNumTel=(TextView)findViewById(R.id.textView_num_tel);
        /*
            affichage des informations récupéré de l'acitivité principal.
         */
        final Bundle bundle = getIntent().getExtras();
        tvNom.setText(getString(R.string.textView_nom)+" "+bundle.getString(MainActivity.EXTRA_NOM));
        tvPrenom.setText(getString(R.string.textView_prenom)+"  "+bundle.getString(MainActivity.EXTRA_PRENOM));
        tvAge.setText((getString(R.string.textView_age)+"  "+bundle.getString(MainActivity.EXTRA_AGE)));
        tvDomComp.setText(getString(R.string.textView_dom_comp)+"  "+bundle.getString(MainActivity.EXTRA_DOM_COMP));
        tvNumTel.setText(getString(R.string.textView_num_tel)+"  "+bundle.getString(MainActivity.EXTRA_NUM_TEL));
        valNumTel=bundle.getString(MainActivity.EXTRA_NUM_TEL);
        /*
            passage vers l'activité 3
         */
        btnOk=(Button)findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentActivity3 =new Intent(Activity2.this,Activity3.class);

                intentActivity3.putExtra(MainActivity.EXTRA_NUM_TEL,valNumTel);
                startActivity(intentActivity3);

            }
        });
        /*
            Retour vers l'activité principale
        */
        btnReturn=(Button)findViewById(R.id.btn_retour);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              finish();
            }
        });



    }
}