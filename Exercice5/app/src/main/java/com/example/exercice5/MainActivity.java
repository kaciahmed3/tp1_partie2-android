package com.example.exercice5;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private Button btnValider;
    final Context context =this;
    EditText ed;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnValider=(Button)findViewById(R.id.btn_valider);
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*
                 Boite de dialogue
                 */
                AlertDialog.Builder myAlertDialogBuilder = new AlertDialog.Builder(context);
                myAlertDialogBuilder.setTitle(getString(R.string.titre_dial));
                myAlertDialogBuilder.setMessage(R.string.msg_dial);
                myAlertDialogBuilder.setCancelable(false);
                myAlertDialogBuilder.setPositiveButton(R.string.oui, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        /*
                         code pour modifier la couleur de fond des edits text
                         */
                        ed =(EditText)findViewById(R.id.editText_nom);
                        ed.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_prenom);
                        ed.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_age);
                        ed.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_dom_comp);
                        ed.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_num_tel);
                        ed.getBackground().setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                myAlertDialogBuilder.setNegativeButton(R.string.non, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        /*
                         code pour modifier la couleur de fond des edits text
                         */
                        ed =(EditText)findViewById(R.id.editText_nom);
                        ed.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_prenom);
                        ed.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_age);
                        ed.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_dom_comp);
                        ed.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                        ed =(EditText)findViewById(R.id.editText_num_tel);
                        ed.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    }
                });
                /*
                     Création de la boite de dialogue
                 */
                AlertDialog myAlertDialog =myAlertDialogBuilder.create();
                /*
                    Affichage de la boite de dialoqgue
                 */
                myAlertDialog.show();
            }
        });
    }
}