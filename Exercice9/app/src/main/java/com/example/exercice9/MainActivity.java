package com.example.exercice9;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText edEvent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edEvent=(EditText)findViewById(R.id.editTextEvent);
        CalendarView calendar=(CalendarView)findViewById(R.id.calendarId);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Toast.makeText(getApplicationContext(),getString(R.string.event)+" : "+dayOfMonth+"/"+month+"/"+year + ":"+edEvent.getText(),Toast.LENGTH_LONG).show();
            }
        });
    }
}