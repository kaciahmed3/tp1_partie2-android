package com.example.exercice8;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    Button btnValidate;
    public static final String EXTRA_DEPARTURE="Departure";
    public static final String EXTRA_ARRIVAL="ARRIVAL";
    private EditText edDeparture;
    private EditText edArrival;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       edDeparture=(EditText)findViewById(R.id.editTextDeparture);
        edArrival=(EditText)findViewById(R.id.editTextArrival);
        btnValidate=(Button)findViewById(R.id.btnValidate);
        /*
            Lancement de l'activité d'affichage quand on click sur le bouton
         */
        btnValidate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentActivity2=new Intent(MainActivity.this,Activity2.class);
                intentActivity2.putExtra(EXTRA_DEPARTURE,edDeparture.getText().toString());
                intentActivity2.putExtra(EXTRA_ARRIVAL,edArrival.getText().toString());
                startActivity(intentActivity2);
            }
        });

    }
}