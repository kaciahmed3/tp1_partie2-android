package com.example.exercice8;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class Activity2 extends AppCompatActivity {

    private ListView listView;
    String departure;
    String arrival;
    TextView element;
    Button btnReturn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        final Bundle bundle=getIntent().getExtras();
        departure=bundle.getString(MainActivity.EXTRA_DEPARTURE);
        arrival=bundle.getString(MainActivity.EXTRA_ARRIVAL);

        element=(TextView)findViewById(R.id.textView1);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 8h:00");

        element=(TextView)findViewById(R.id.textView2);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 8h:30");

        element=(TextView)findViewById(R.id.textView3);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 9h:00");

        element=(TextView)findViewById(R.id.textView4);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 9h:30");

        element=(TextView)findViewById(R.id.textView5);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 10h:00");

        element=(TextView)findViewById(R.id.textView6);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 10h:30");

        element=(TextView)findViewById(R.id.textView7);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 11h:00");

        element=(TextView)findViewById(R.id.textView8);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 11h:30");

        element=(TextView)findViewById(R.id.textView9);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 12h:00");

        element=(TextView)findViewById(R.id.textView10);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 12h:30");

        element=(TextView)findViewById(R.id.textView11);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 13h:00");

        element=(TextView)findViewById(R.id.textView12);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 13h:30");

        element=(TextView)findViewById(R.id.textView13);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 14h:00");

        element=(TextView)findViewById(R.id.textView14);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 14h:30");

        element=(TextView)findViewById(R.id.textView15);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 15h:00");

        element=(TextView)findViewById(R.id.textView16);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 15h:30");

        element=(TextView)findViewById(R.id.textView17);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 16h:00");

        element=(TextView)findViewById(R.id.textView18);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 16h:30");

        element=(TextView)findViewById(R.id.textView19);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 17h:00");
        element=(TextView)findViewById(R.id.textView20);
        element.setText(getString(R.string.textViewDeparture)+" "+departure+" , "+getText(R.string.textViewArrival)+" "+arrival+" 18h:00");

        /*
            Retour vers l'activité principale
        */
        btnReturn=(Button)findViewById(R.id.btn_retour);
        btnReturn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}